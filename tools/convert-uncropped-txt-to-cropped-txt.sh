#!/bin/sh

for file in ./txt/*.txt; do
    echo Converting file $file...
    python3 ./tools/crop.py $file | tee temp
    mv temp $file
done

