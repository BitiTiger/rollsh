#!/bin/sh

mkdir txt
for jpgfile in ./jpg/*.jpg; do
    echo Converting file $jpgfile...
    txtfile=${jpgfile//jpg/txt}
    jp2a $jpgfile --background=dark --size=80x25 | tee $txtfile
done

