# Don't forget to change the path inside rollsh if you change FILE_PATH
FILE_PATH=/usr/local/share/rollsh
EXEC_FILE=/usr/local/bin/rollsh

all:
	make video
	make jpg
	make txt

video:
	./tools/download_video.sh

jpg:
	./tools/convert-video-to-jpg.sh

txt:
	./tools/convert-jpg-to-txt.sh
	./tools/convert-uncropped-txt-to-cropped-txt.sh

clean:
	rm -rf txt jpg "Rick Astley - Never Gonna Give You Up (Official Music Video)-dQw4w9WgXcQ.mkv"

install:
	sudo mkdir $(FILE_PATH)
	sudo mkdir $(FILE_PATH)/txt
	sudo cp ./rollsh $(FILE_PATH)/rollsh
	sudo cp ./roll.py $(FILE_PATH)/roll.py
	sudo cp -r ./txt/* $(FILE_PATH)/txt/
	sudo ln -s $(FILE_PATH)/rollsh $(EXEC_FILE)

uninstall:
	sudo rm -f $(EXEC_FILE)
	sudo rm -rf $(FILE_PATH)

